<?php

namespace App\Http\Controllers\AuthUser;

use App\AuthUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthUserController extends Controller
{
    public function store(Request $request)
    {
        // Create Auth User
        $user = AuthUser::createAuthUser($request['name'], $request['email'], $request['privilage'], $request['password']);

    	return back();
    }
}
