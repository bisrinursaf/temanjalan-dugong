<?php

namespace App\Http\Controllers\Conversation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LinebotLog;
use MongoDB\BSON\Regex as MongoRegex;

class ConversationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
    	$path  = 'conversations';

        $messages = [];

    	$threads = LinebotLog::with('User')
                ->where('message.text','regexp', new MongoRegex('.*Maaf respon*.'))
                ->orderBy('created_at', 'desc')
    			->take(10)
                ->get();
        //return $threads[0];
    	return view('conversations.index', compact('path', 'threads', 'messages'));
    }
}
