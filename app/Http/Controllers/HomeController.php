<?php

namespace App\Http\Controllers;

use App\AuthUser;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $path = 'auth-user';
        $users = AuthUser::all();
        
        return view('auth_users.index', compact('path', 'users'));                
    }
    
}
