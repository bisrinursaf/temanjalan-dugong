<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $path = 'users';
        $users = User::all();
        
        return view('users.index', compact('path', 'users'));                
    }

    public function detail($uid)
    {
        $path  = 'users-detail';
        $user = User::where('uid',$uid)->get();

        return view('users.detail', compact('path','user'));                                
    }
}
