<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\User;

class LinebotLog extends Eloquent
{
    protected $collection = 'linebot_logs';

    function user() {
    	return $this->belongsTo('App\User', 'destination.destinationId', 'line_mid');
    }

    public function getUserIdAttribute() {
    	return $this->user->uid;
    }
}
