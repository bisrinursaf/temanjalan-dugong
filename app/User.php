<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'users';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = '_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'id_number', 'email', 'name','nickname', 'department','sub_department', 'phone','wa_available', 
        'line_available', 'line_id', 'line_mid', 'line_group', 'line_displayname',
        'line_picture_url', 'line_picture_filename','linebot_state', 'linebot_mute','linebot_prev_state', 
        'role', 'pref_role', 'point', 'remaining_trip', 'finished_trip','vote_reward', 'referrer_uid','gender', 
        'created_at'
    ];

    public function getProfilePictureAttribute() {
        return \Config::get('constants.dropbox_base_url').$this->line_picture_filename.'_thumb.jpg';
    }
}