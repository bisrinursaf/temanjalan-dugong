@extends('layouts.admin.auth-master')

@section('adminBodyClass', 'auth login')

@section('content')

 <div class="content-block">

        <div class="logo-wrap">
            <a href="/" class="logo"></a>
            <p>DUGONG</p>
        </div>
        <form role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <div class="row form">
                <div class="col lg-12">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input name="email" type="text" class="form-control lg" placeholder="Email or username..." required autofocus>
                    </div>
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input name="password" type="password" class="form-control lg" placeholder="Password..." required>
                    </div>
                </div>
            </div>
            <div class="row mt-30 mb-30">
                <div class="col xs-6 sm-6 md-6 lg-6 xlg-6 pl-40">
                    <button type="submit" class="mb-5 button block round md success">Login</button>
                </div>
            </div>
        </form>

 
    </div>
@endsection