@extends('layouts.admin.auth-master')

@section('adminBodyClass', 'auth login')

@section('content')

    <div class="content-block register">

        <div class="logo-wrap">
            <a href="/" class="logo">
            </a>
            <p>DUGONG</p>
        </div>
        <form action="">
            {{ csrf_field() }}
        
            <div class="row form register">
                <div class="col lg-4">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control lg" placeholder="First name...">
                    </div>
                </div>
                <div class="col lg-4">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control lg" placeholder="Middle name...">
                    </div>
                </div>
                <div class="col lg-4">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control lg" placeholder="Last name...">
                    </div>
                </div>
            </div>
            <div class="row form register">
                <div class="col lg-12">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="password" class="form-control lg" placeholder="Email...">
                    </div>
                </div>
            </div>
            <div class="row form register">
                <div class="col lg-6">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control lg" placeholder="Password...">
                    </div>
                </div>
                <div class="col lg-6">
                    <div class="input-group justify lg">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control lg" placeholder="Confirm password...">
                    </div>
                </div>
            </div>
            <div class="row form register">
                <div class="col xs-6 sm-6 md-6 lg-6 xlg-6 text-center">
                    <button class="mb-5 button round md success">Register</button>
                </div>
                <div class="col xs-6 sm-6 md-6 lg-6 xlg-6 text-center">
                    <a href="/" class="mb-5 button round md warning">Cancel</a>
                </div>
            </div>
        </form>

       
    </div>

@endsection