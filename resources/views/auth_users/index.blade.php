@extends('layouts.admin.master')

@section('content')

    @include('admin.page-title',['pageTitle' => 'Users'])

    <div class="container">
        <div class="row">
            <div class="col lg-12">
                <section class="box">
                    <header class="box-header info">
                        <h2 class="box-header-title pull-left">User Management</h2>
                    </header>

                    <!-- Show User -->
                    <main class="box-content">
                    <div class="row mt-0">
                        <div class="col lg-8">
                            <div class="responsive-table-wrap">
                                <table class="table responsive">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Privilage</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user-> name }}</td>
                                        <td>{{ $user-> email }}</td>
                                        <td>{{ $user-> privilage }}</td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                        </div>
                    </div>
                    </main>

                    <!-- Create New User -->
                    @if(Auth::user()->privilage === 'Admin')
                    <form role="form" action="user/store" method="POST">
                        <div class="row">
                            <div class="col lg-3">
                                <a href="#" data-modal-id="modal-1" class="btn btn-primary modal-button">Create New User</a>
                                    <div id="modal-1" class="modal">
                                        <div class="dialog">

                                            <div class="heading">
                                                <a class="close" href="#" data-close-modal><i class="mdi mdi-window-close"></i></a>
                                            </div>

                                            <div class="body">
                                                    {{ csrf_field() }}

                                                    <p>Name</p>
                                                    <input name="name" type="text" class="form-control lg" placeholder="Name..." required autofocus>

                                                    <p>Email</p>
                                                    <input type="email" name="email" class="form-control lg" placeholder="Email...">

                                                    <p>Privilage</p>
                                                    <div class="form-group">
                                                        <select name="privilage" id="select-form-1" class="form-control md">
                                                            <option value="Admin">Admin</option>
                                                            <option value="Default">Default</option>
                                                        </select>
                                                    </div>

                                                    <p>Password</p>
                                                    <input name="password" type="password" class="form-control lg" placeholder="Password..." required>
                                            </div>

                                            <div class="footer">
                                                <div class="pull-right">
                                                    <button type="submit" class="btn md primary ml-10">REGISTER</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                        </div>
                    </form>
                    @endif

                </section>
            </div>
        </div>
    </div>

@endsection