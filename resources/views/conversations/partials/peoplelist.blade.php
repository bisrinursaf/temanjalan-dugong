<div class="people-list" id="people-list">
    <div class="search" style="text-align: center">
        <a href="{{url('/home')}}" style="font-size:16px; text-decoration:none; color: white;"><i class="fa fa-user"></i> {{auth()->user()->name}}</a>
    </div>
    <ul class="list">
        @foreach($threads as $inbox)
            @if(!is_null($inbox->message['text']))
        <li class="clearfix">
            <a href="#">
            <img src="{{ $inbox->user->profile_picture }}" alt="avatar" />
            <div class="about">
                <div class="name">{{$inbox->user->name}}</div>
                <div class="status">
                    <span class="fa fa-reply"></span>
                    <span>{{substr($inbox->message['text'], 0, 20)}}</span>
                </div>
            </div>
            </a>
        </li>
            @endif
        @endforeach

    </ul>
</div>
