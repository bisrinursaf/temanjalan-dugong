<div id="main-menu" class="main-menu">
    <div class="main-menu-profile-row">
        <div class="main-menu-profile-img">
            <a href="/"><img src="{{ asset('images/users/1.png') }}" alt=""></a>
        </div>

        <div class="main-menu-profile-info">
            <h5 class="title"><a href="/">{{ Auth::user()->name }}</a></h5>
            <p class="role">{{ Auth::user()->privilage }}</p>
            
            <!-- Logout -->
            <a href="{{ url('/logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <p>Logout</p>
            </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
            </form>
        </div>
    </div>

    <div class="main-menu-links-row">
        <ul id="main-menu" class="main-menu perform-menu">
            <li class="{{ adminMenuClass('auth-user', $path) }}">
                <a href="{{ route('auth-user') }}">
                    <i class="fa fa-user"></i> Auth User Management
                </a>
            </li>
            <li class="{{ adminMenuClass('users', $path) }}">
                <a href="{{ route('users') }}">
                    <i class="fa fa-users"></i> Users
                </a>
            </li>
            <li class="{{ adminMenuClass('conversations', $path) }}">
                <a href="{{ route('conversations') }}">
                    <i class="fa fa-wechat"></i> Conversations   
                </a>
            </li>
         </ul>
    </div>
</div>