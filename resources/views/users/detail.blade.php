@extends('layouts.admin.master')

@section('content')

    @include('admin.page-title',['pageTitle' => 'Users'])

        @foreach($user as $user)
            <li>{{ $user->name }}</li>
            <li>{{ $user->nickname }}</li>
            <li>{{ $user->email }}</li>
            <li>{{ $user->department }}</li>
            <li>{{ $user->sub_department }}</li>
            <li>{{ $user->line_id }}</li>
            <li>{{ $user->line_displayname }}</li>
            <li>{{ $user->line_picture_url }}</li>
            <li>{{ $user->gender }}</li>
        @endforeach

@endsection