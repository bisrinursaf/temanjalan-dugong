@extends('layouts.admin.master')

@section('content')

    @include('admin.page-title',['pageTitle' => 'Users'])

     <div class="container">
            <div class="row">
                <div class="col lg-12">
                    <section class="box">
                        <header class="box-header info">
                            <h2 class="box-header-title pull-left">User Management</h2>
                        </header>

                        <!-- Show User -->
                        <main class="box-content">
                        <div class="row mt-0">
                            <div class="col lg-12">
                                <div class="responsive-table-wrap">
                                    <table class="table responsive">
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Line ID</th>
                                            <th>Gender</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($users as $user)
                                        <tr>
                                            <td><a href="users/detail/{{ $user-> uid }}">{{ $user-> uid }}</a></td>
                                            <td>{{ $user-> name }}</td>
                                            <td>{{ $user-> email }}</td>
                                            <td>{{ $user-> line_id }}</td>
                                            <td>{{ $user-> gender }}</td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </main>
                    </section> 
                </div>
            </div>
    </div>                         
                        

@endsection