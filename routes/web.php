<?php

/*=== Authentication ===*/
Auth::routes();

Route::get('/', 'HomeController@index');

/*=== Auth User ===*/
Route::get('auth-users', 'HomeController@index')->name('auth-user');
Route::post('user/store', 'AuthUser\AuthUserController@store');

/*=== User ===*/
Route::get('users', 'User\UserController@index')->name('users');
Route::get('users/detail/{uid}' , 'User\UserController@detail');

/*=== Live Chat ===*/
Route::get('conversations', 'Conversation\ConversationController@index')->name('conversations');