<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;

class LoginTest extends TestCase
{

	public function testLogin()
	{
		$this->visit('/');
		$this->type('bisrinursa@gmail.com','email');
		$this->type('123456','password');
		$this->see("Login");
		$this->press('Login');
		$this->seePageIs('login');
	}
}