<?php

use App\AuthUser;

class AuthUserTest extends PHPUnit_Framework_TestCase
{
	protected $user;

	public function setUp()
	{		
		$this->user = new AuthUser([
			'_id' => '586dbb57837df980a6b6667b',
			'password' => '$2y$10$WDtqDSN6ApskjkV0j/iwT.xOA527UT2hGNyN1hSMnmWJ0B.xBE13a',
			'name' => 'bisri',
			'email' => 'bisrinursa@gmail.com',
			'privilage' => 'Admin',
			'created_at' => '2016-10-30 13:55:37'
			]);
	}

	/** @test */
	function auth_user_HasPassword()
	{
		$this->assertEquals('$2y$10$WDtqDSN6ApskjkV0j/iwT.xOA527UT2hGNyN1hSMnmWJ0B.xBE13a', $this->user->password);
	}

	/** @test */
	function auth_user_HasName()
	{
		$this->assertEquals('bisri', $this->user->name);
	}

	/** @test */
	function auth_user_HasEmail() 
	{
		$this->assertEquals('bisrinursa@gmail.com', $this->user->email);
	}

	/** @test */
	function auth_user_HasPrivilage()
	{
		$this->assertEquals('Admin', $this->user->privilage);
	}

	/** @test */
	function auth_user_HasCreatedat()
	{
		$this->assertEquals('2016-10-30 13:55:37' , $this->user->created_at);
	}

}