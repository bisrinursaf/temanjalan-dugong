<?php

use App\User;

class UserTest extends PHPUnit_Framework_TestCase
{
	protected $user;

	public function setUp()
	{
		$this->user = new User([
			'_id' => 'ObjectId("586f6dae8a8481ee044509f9")',
			'uid' => '24',
			'id_number' => '1606822655',
			'email' => 'email@gmail.com',
			'name' => 'Name',
			'nickname' => 'Nickname',
			'department' => 'Fakultas Kesehatan Masyarakat',
			'sub_department' => 'Kesehatan Masyarakat',
			'phone' => 'NULL',
			'wa_available' => 'NULL',
			'line_available' => 'NULL',
			'line_id' => 'NULL',
			'line_mid' => 'NULL',
			'line_group' => 'NULL',
			'line_displayname' => 'NULL',
			'line_picture_url' => 'NULL',
			'line_picture_filename' => 'NULL',
			'linebot_state' => '0',
			'linebot_mute' => '0',
			'linebot_prev_state' => 'NULL',
			'role' => '0',
			'pref_role' => 'NULL',
			'point'=> '3',
			'remaining_trip' => 'NULL',
			'finished_trip' => '5',
			'vote_reward' => 'NULL',
			'referrer_uid' => '2',
			'gender' => '1',
			'created_at' => '2016-10-30 13:55:37'
			]);
	}

	/** @test */
	function user_HasUid()
	{
		$this->assertEquals('24', $this->user->uid);
	}

	/** @test */
	function user_HasIdnumber()
	{
		$this->assertEquals('1606822655', $this->user->id_number);
	}

	/** @test */
	function user_HasEmail()
	{
		$this->assertEquals('email@gmail.com', $this->user->email);
	}

	/** @test */
	function user_HasName()
	{
		$this->assertEquals('Name', $this->user->name);
	}

	/** @test */
	function user_HasNickname()
	{
		$this->assertEquals('Nickname', $this->user->nickname);
	}

	/** @test */
	function user_HasDepartment()
	{
		$this->assertEquals('Fakultas Kesehatan Masyarakat', $this->user->department);
	}

	/** @test */
	function user_HasSubdepartment()
	{
		$this->assertEquals('Kesehatan Masyarakat', $this->user->sub_department);
	}

	/** @test */
	function user_HasPhone()
	{
		$this->assertEquals('NULL', $this->user->phone);
	}

	/** @test */
	function user_HasWaavalailable()
	{
		$this->assertEquals('NULL', $this->user->wa_available);
	}

	/** @test */
	function user_HasLineavailable()
	{
		$this->assertEquals('NULL', $this->user->line_available);
	}

	/** @test */
	function user_HasLineid()
	{
		$this->assertEquals('NULL', $this->user->line_id);
	}

	/** @test */
	function user_HasLinemid()
	{
		$this->assertEquals('NULL', $this->user->line_mid);
	}

	/** @test */
	function user_HasLinegroup()
	{
		$this->assertEquals('NULL', $this->user->line_group);
	}

	/** @test */
	function user_HasLinedisplayname()
	{
		$this->assertEquals('NULL', $this->user->line_displayname);
	}

	/** @test */
	function user_HasLinepictureurl()
	{
		$this->assertEquals('NULL', $this->user->line_picture_url);
	}

	/** @test */
	function user_HasLinepicturefilename()
	{
		$this->assertEquals('NULL', $this->user->line_picture_filename);
	}

	/** @test */
	function user_HasLinebotstate()
	{
		$this->assertEquals('0', $this->user->linebot_state);
	}

	/** @test */
	function user_HasLinebotmute()
	{
		$this->assertEquals('0', $this->user->linebot_mute);
	}

	/** @test */
	function user_HasLinebotprevstate()
	{
		$this->assertEquals('NULL', $this->user->linebot_prev_state);
	}

	/** @test */
	function user_HasRole()
	{
		$this->assertEquals('0', $this->user->role);
	}

	/** @test */
	function user_HasPrefrole()
	{
		$this->assertEquals('NULL', $this->user->pref_role);
	}

	/** @test */
	function user_HasPoint()
	{
		$this->assertEquals('3', $this->user->point);
	}

	/** @test */
	function user_HasRemainingtrip()
	{
		$this->assertEquals('NULL', $this->user->remaining_trip);
	}

	/** @test */
	function user_HasFinishedtrip()
	{
		$this->assertEquals('5', $this->user->finished_trip);
	}

	/** @test */
	function user_HasVotereward()
	{
		$this->assertEquals('NULL', $this->user->vote_reward);
	}

	/** @test */
	function user_HasReferreruid()
	{
		$this->assertEquals('2', $this->user->referrer_uid);
	}

	/** @test */
	function user_HasGender()
	{
		$this->assertEquals('1', $this->user->gender);
	}
		
	/** @test */
	function user_HasCreatedat()
	{
		$this->assertEquals('2016-10-30 13:55:37' , $this->user->created_at);
	}

}